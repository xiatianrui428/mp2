import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import ListView from './Component/ListView/ListView'
import Gallery from './Component/Gallery/Gallery'
import NavBar from './Component/NavBar/NavBar';

function App() {
  return (
  <div className="app">
    <NavBar/ >
    <Route path="/ListView" component={ListView} />
    <Route path="/Gallery" component={Gallery} />
    <div className="footer">
      <img src={"https://www.themoviedb.org/assets/2/v4/logos/primary-blue-40c00543e47b657e8e53a2f3e8650eb9de230316cf158965edb012d72ddca755.svg"}/>
      <div>Data provided by © 2014 THE MOVIE DB</div>
    </div>
  </div>
  );
}

export default App;
