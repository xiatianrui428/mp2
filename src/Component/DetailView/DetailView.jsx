import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import './DetailView.scss';
import { Button, Grid } from '@material-ui/core';
import { REACT_APP_IMG } from '../../Constants/Constants'

const DetailView = props => {
    const { curIndex, setIndex, setDown, open, allData } = props;
    let data = allData[curIndex];
    console.log(data);
    return (
        <Modal
            open={open}
            onClose={()=>setDown(false)}>
            <div className="wrapper">

                <div className="container">
                    <Button disabled={curIndex===0} onClick={()=>setIndex(curIndex - 1)}>
                        Prev
                    </Button>
                    {data ? (<div className="mainView">
                                    <img src={REACT_APP_IMG + data.poster_path}></img>
                                        <div className="detail">
                                            <h3>{data.title}</h3>
                                            <div>
                                                <h5>Overview: </h5>
                                                <p>{data.overview}</p>
                                            </div>

                                            <div>
                                                <h5>Rating: </h5>
                                                <p> {` ${data.vote_average} / 10`}</p>
                                            </div>
                                            <div>
                                                <h5>Release Date: </h5>
                                                <p>{data.release_date}</p>
                                            </div>
                                            

                                            <Button onClick={()=>setDown(false)}>CLOSE</Button> 
                                        </div>
                                </div>)
                        : <p>Nothing</p> 
                    }
                    <Button disabled={curIndex===allData.length - 1} 
                                onClick={()=>setIndex(curIndex + 1)}>
                        Next
                    </Button>
                </div>
            </div>
        </Modal>
    )
};

DetailView.propTypes = {
    curIndex: PropTypes.number.isRequired,
    setIndex: PropTypes.func.isRequired,
    setDown: PropTypes.func.isRequired,
    allData: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
};

export default DetailView;
