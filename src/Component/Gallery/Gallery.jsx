import React, {useEffect, useState} from 'react';
import Rating from '@material-ui/lab/Rating';
import { Card, CardContent, TextField, Radio, FormControlLabel, CardActionArea, CardMedia, Typography, Button } from '@material-ui/core';
import axios from 'axios';
import DetailView from '../DetailView/DetailView';
import {REACT_APP_ENDPOINT, REACT_APP_API_KEY, REACT_APP_IMG} from '../../Constants/Constants';
import './Gallery.scss'


const endpoint = REACT_APP_ENDPOINT
const key = REACT_APP_API_KEY;

const CATEGORIES = ['Top rated', 'Upcoming' , 'Now playing' ,'Popular']
const ROUTE = ['top_rated', 'upcoming', 'now_playing', 'popular'];

const Gallery = () => {
    const [info, setInfo] = useState([]);
    const [category, setCategory] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [cur, setCur] = useState(0);
    const [open, setOpen] = useState(false);

    useEffect(()=>{
        var url =  endpoint + `/${ROUTE[category]}?api_key=${key}&page=${currentPage}`; 
        axios.get(url).then((data)=>{
            setInfo(data.data.results)
            console.log(data);
        }).catch((error) => {
            console.log(error);
        })
    },[category, currentPage]);
    
    var display = info.slice(0,20);
    var child = display.map((each, index) => (
           <Card raised key={index}>
            <CardActionArea onClick={()=>{setCur(index); setOpen(true);}}>
                <CardMedia
                    component="img"
                    width={100}
                    image={REACT_APP_IMG + each.poster_path}/>
                <CardContent>
                    <Typography variant="h5">
                        {`${20 * (currentPage - 1) + index + 1}. `+ each.title + ` (${each.release_date.substring(0,4)})`}
                    </Typography>
                    {
                        /* 
                            <Typography component="p" variant="body2">
                                {each.overview.substring(0,100) + "..."}    
                            </Typography> 
                        */
                    }
                    
                    <Rating value={each.vote_average / 2} readOnly></Rating>
                    <Typography component="legend">{` ${each.vote_average}/10`}</Typography>
                </CardContent>
            </CardActionArea>
           </Card>
    ))
    var radios = CATEGORIES.map((each, index)=>
        <FormControlLabel label={each} control={
            <Radio
                name={each}
                checked={index === category}
                onChange={()=>{
                    setCategory(index);
                }}
            />} />
        
    )

    var footer = currentPage === 1 ? [] : [<Button onClick={()=>setCurrentPage(currentPage - 1)}>Prev</Button>];
    var startPage = currentPage <= 5 ? 1 :currentPage - 5;
    for(let i = 0; i < 10; i += 1) {

        if(startPage + i === currentPage) {
            footer.push(<a className="pageNumber active"> {startPage + i }</a>)
        } else {    
            footer.push(<span className="pageNumber" onClick={()=>setCurrentPage(startPage + i)}> {startPage + i}</span>)
        }
    }

    footer.push(<Button onClick={()=>setCurrentPage(currentPage + 1)}>Next</Button>)
    return (
        <div className="gallery">
            <div>
                {radios}
            </div>
            <div className="cardView">

                {child}
            </div>
            <div >
                {footer}
            </div>
            <DetailView open={open} 
                        setDown={setOpen} 
                        curIndex={cur}
                        setIndex={setCur}
                        allData={info}/>
        </div>
    )
};

Gallery.propTypes = {

};

export default Gallery;
