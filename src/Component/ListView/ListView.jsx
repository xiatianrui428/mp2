import React, {useState} from 'react';
import DetailView from '../DetailView/DetailView'
import Axios from 'axios';
import { 
    TextField, 
    Grid, 
    IconButton, 
    Paper, 
    Button,
    FormControl, 
    InputLabel, 
    Select, 
    Dialog, 
    DialogTitle, 
    DialogContent, 
    Input, 
    ListItem, 
    ListItemText, 
    CircularProgress, 
    ListItemAvatar,
    Avatar
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import FilterListIcon from '@material-ui/icons/FilterList';
import { REACT_APP_IMG, REACT_APP_SEARCH} from '../../Constants/Constants';
import './ListView.css';


const ListView = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [cur, setCur] = useState(0);
    const [filter, setFilter] = useState("");
    const [open, setOpen] = useState(false);
    const [data, setData] = useState([]);
    const [searching, setSearching] = useState(false);
    const [pages, setPages] = useState(0);
    const [type, setType] = useState(10);
    const [order, setOrder] = useState(10);
    const [down, setDown] = useState(false);
    const filterMovies = () => {
        Axios.get(REACT_APP_SEARCH + filter).then((info)=>{
            setData(info.data.results);
            setPages(info.data.total_pages > 10 ? 10 : info.data.total_pages);
            setSearching(false);  
        }).catch((error)=> {
            console.error(error);
        })
    }

    React.useEffect(()=> {
        Axios.get(REACT_APP_SEARCH+filter+`&page=${currentPage}`).then((data)=> {
            setData(data.data.results);
        }).catch((err)=>{
            console.error(err);
        })
    },[currentPage])

    const handleType = (e) => {
        setType(e.target.value);   
    }

    const handleOrder = (e) => {
        setOrder(e.target.value);
    }

    const nameComparatorAscending = (a, b) => {
        const t1 = a.title.toUpperCase().substring(0,1);
        const t2 = b.title.toUpperCase().substring(0,1);
        if(t1 > t2) {
            return 1
        } else if(t1 < t2) {
            return -1;
        } else {
            return 0
        }
    }

    const nameComparatorDecending = (a, b) => {
        const t1 = a.title.toUpperCase().substring(0,1);
        const t2 = b.title.toUpperCase().substring(0,1);
        if(t1 < t2) {
            return 1
        } else if(t1 > t2) {
            return -1;
        } else {
            return 0
        }
    }

    const rankComparatorAscending = (a, b) => {
        const r1 = a.vote_average;
        const r2 = b.vote_average;
        if(r1 < r2) {
            return 1;
        } else if(r1 > r2) {
            return -1;
        } else {
            return 0;
        }
    }

    const rankComparatorDescending = (a, b) => {
        const r1 = a.vote_average;
        const r2 = b.vote_average;
        if(r1 > r2) {
            return 1;
        } else if(r1 < r2) {
            return -1;
        } else {
            return 0;
        }
    }

    const filterMovie = (curType, curOrder) => {
        console.log("filteringMovie");
        console.log(curType )
        console.log(curOrder);
        var sorted;

        if(curType == 10) {
            if(curOrder == 10) {
                sorted = data.sort(rankComparatorAscending);
            } else if(curOrder == 20) { 
                sorted = data.sort(rankComparatorDescending);
            }
        } else if(curType == 20) {
            if(curOrder == 10) {
                sorted = data.sort(nameComparatorAscending);
            } else if(curOrder == 20) {   
                sorted = data.sort(nameComparatorDecending);
            }
        }
        console.log(sorted);
        setData(sorted);
    }

    var movies = data.map((each, index) => 
    <ListItem button onClick={()=> { setDown(true); setCur(index);}} key={index}> 
        <ListItemAvatar large
         >
            <Avatar src={REACT_APP_IMG + each.poster_path}/>
        </ListItemAvatar>
        <h4>{`${(20 * (currentPage - 1)) + index + 1}. `+ each.title} </h4>
        <ListItemText className="second" primary={` avg. rating: ${each.vote_average}`}/>
    </ListItem>
    );
    

    let pageIndex = [<Button disabled={currentPage===1} onClick={()=>setCurrentPage(currentPage - 1)}>Prev</Button>];
    for(let i = 1; i <= pages; i += 1) {
        if(i === currentPage) {
            pageIndex.push(<span className="pageNumber active"> {i}</span>)
        } else {    
            pageIndex.push(<span className="pageNumber" onClick={()=>setCurrentPage(i)}> {i}</span>)
        }
    }
    pageIndex.push(<Button disabled={currentPage===pages} onClick={()=>setCurrentPage(currentPage + 1)}>Next</Button>)
    

    movies.push(<div className="pageIndex">{pageIndex}</div>);
    movies = (movies.length === 1) ? <p className="prompt">We can't find any matching moviews</p> : movies;
    movies = filter === "" ? <p className="prompt"> Type movie name to search </p> : movies;
    movies = searching || open ? <CircularProgress /> : movies;
    
    
    var container = (
        <Paper elevation={2} >
            <div className="toolBar">
                <h3>
                    Movie List
                </h3>
                <div className="filter">
                    <p>
                        Filter Options
                    </p>
                    <IconButton onClick={()=>{setOpen(!open); setFilter(type, order);}}>
                        <FilterListIcon></FilterListIcon>
                    </IconButton>
                </div>
            </div>
            <div className="scrollList"> { movies } </div>
            
            <Dialog open={open} maxWidth="xs">
                <DialogTitle> Filter Option </DialogTitle>
                <DialogContent >
                    <div className="form-control" >
                        <FormControl >
                            <InputLabel htmlFor="demo-dialog-native">Filter By: </InputLabel>
                            <Select
                                onChange={handleType}
                                native
                                value={type}
                                input={<Input id="demo-dialog-native" />}
                            >
                                <option value={10}>Ranking</option>
                                <option value={20}>Name</option>
                            </Select>

                        </FormControl>
                        <FormControl >
                        <InputLabel htmlFor="demo-dialog-native">Order By: </InputLabel>
                        <Select
                            onChange={handleOrder}
                            native
                            value={order}
                            input={<Input id="demo-dialog-native" />}
                        >
                            <option value={10}>Ascending</option>
                            <option value={20}>Descending</option>
                        </Select>

                    </FormControl>
                    </div>
                
                    <Button onClick={()=>{setOpen(false);filterMovie(type, order)}}>Comfirm</Button>
                </DialogContent>
            </Dialog>
        </Paper>
    )
        
    
    return (
        <div className="listView">
            <div className="searchBar">
                <Grid container spacing={1} alignItems="flex-end">
                    <Grid item xs={11}>
                        <TextField variant="outlined" onChange={(e)=>{setFilter(e.target.value); setSearching(true);}}fullWidth={true} label="Enter A Movie Name" />
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton onClick={()=>{filterMovies()}}>
                            <SearchIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </div>
            {container}
            <DetailView open={down}  
                        setDown={setDown} 
                        curIndex={cur}
                        setIndex={setCur}
                        allData={data}/>
        </div>
    )
};

ListView.propTypes = {

};




export default ListView;
