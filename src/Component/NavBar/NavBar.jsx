import React from 'react';
import './NavBar.scss'
import { Link } from 'react-router-dom';
function NavBar() {
    return (
    <div className="navBar">
        <h2>The Top 100 IMDB Movies</h2>
        <Link to="/ListView"> ListView </Link>
        <Link to="/Gallery"> Gallery </Link>
      </div>
    )
}

export default NavBar;
